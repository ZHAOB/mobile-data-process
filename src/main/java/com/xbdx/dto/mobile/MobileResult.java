package com.xbdx.dto.mobile;

import lombok.Data;

import java.util.List;

@Data
public class MobileResult {
    public String iotId;
    public String iotName;
    public String iotPoi;
    public String iotType;
    public List<String> switchStatus;
    public String temp;
    public String moi;
}
