package com.xbdx.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@Table(name = "bsn_device_temp")
public class DeviceTemp {


    @Id
    @Column(name = "iot_id")
    public String iotId;

    @Column(name = "app_id")
    public String appId;
    @Column(name = "app_name")
    public String appName;
    @Column(name = "group_id")
    public String groupId;
    @Column(name = "group_name")
    public String groupName;
    @Column(name = "hardVer")
    public String hardVer;
    @Column(name = "lastTime")
    public Timestamp lastTime;
    @Column(name = "name")
    public String name;
    @Column(name = "onlineTime")
    public Timestamp onlineTime;
    @Column(name = "root_group_id")
    public String rootGroupId;
    @Column(name = "root_group_name")
    public String rootGroupName;
    @Column(name = "softVer")
    public String softVer;
    @Column(name = "states")
    public String states;
    @Column(name = "success")
    public Boolean success;
    @Column(name = "iot_type")
    public String iotType;
    @Column(name = "insert_time")
    public Timestamp insertTime;
    @Column(name = "is_enable")
    public Integer isEnable;
    @Column(name = "type_id")
    public String typeId;
    @Column(name="space_id")
    public Integer spaceId;
    @Column(name="space_name")
    public String spaceName;


    //Data_
    //温度
    @Column(name = "data_temperature_1")
    public Double dataTemperature1;
    @Column(name = "Data_ENUM_1")
    public Double dataEnum1;
    @Column(name = "Data_LUX_1")
    public Double dataLux1;
    //湿度
    @Column(name = "Data_MOISTURE_1")
    public Double dataMoisture1;


    //电流
    @Column(name = "Data_ELECTRICITY_1")
    public Double dataElectricity1;
    //功率
    @Column(name = "Data_POWERS_1")
    public Double dataPowers1;
    //电量
    @Column(name = "Data_QUANTITY_1")
    public Double dataQuantity1;
    @Column(name = "Data_STRING_1")
    public Double dataString1;
    //电压
    @Column(name = "Data_VOLTAGE_1")
    public Double dataVoltage1;

    //Data_CloudState_
    //开关状态"0": "关闭",W"1": "打开"
    @Column(name = "Data_CloudState_DEV_SWITCH_STA_1")
    public Double dataCloudstateDevSwitchSta1;
    @Column(name = "Data_CloudState_DEV_SWITCH_STA_2")
    public double dataCloudstateDevSwitchSta2;
    @Column(name = "Data_CloudState_DEV_SWITCH_STA_3")
    public double dataCloudstateDevSwitchSta3;
    @Column(name = "Data_CloudState_ENUM_1")
    public Double dataCloudstateEnum1;
    @Column(name = "Data_CloudState_ENUM_2")
    public Double dataCloudstateEnum2;
    @Column(name = "Data_CloudState_ENUM_3")
    public Double dataCloudstateEnum3;
    @Column(name = "Data_CloudState_ENUM_4")
    public Double dataCloudstateEnum4;

    //Data_SyncState_
    //开关状态"0": "关闭",W"1": "打开"
    @Column(name = "Data_SyncState_DEV_SWITCH_STA_1")
    public Double dataSyncstateDevSwitchSta1;
    @Column(name = "Data_SyncState_DEV_SWITCH_STA_1_BetaTime")
    public Timestamp dataSyncstateDevSwitchSta1Betatime;
    @Column(name = "Data_SyncState_ENUM_1")
    public Double dataSyncstateEnum1;
    @Column(name = "Data_SyncState_ENUM_1_BetaTime")
    public Timestamp dataSyncstateEnum1Betatime;
    @Column(name = "Data_SyncState_ENUM_2")
    public Double dataSyncstateEnum2;
    @Column(name = "Data_SyncState_ENUM_2_BetaTime")
    public Timestamp dataSyncstateEnum2Betatime;
    @Column(name = "Data_SyncState_ENUM_3")
    public Double dataSyncstateEnum3;
    @Column(name = "Data_SyncState_ENUM_3_BetaTime")
    public Timestamp dataSyncstateEnum3Betatime;
    @Column(name = "Data_SyncState_ENUM_4")
    public Double dataSyncstateEnum4;
    @Column(name = "Data_SyncState_ENUM_4_BetaTime")
    public Timestamp dataSyncstateEnum4Betatime;

}
