package com.xbdx.controller;

import com.xbdx.dao.DeviceSwitchDao;
import com.xbdx.model.DeviceSwitch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping(value = "/mobile")
@CrossOrigin()
public class MobileController {

    @GetMapping("/iotList")
    @ResponseBody
    public String iotList() {
        return "iotList";
    }

}
