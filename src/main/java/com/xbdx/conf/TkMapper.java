package com.xbdx.conf;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * Mapper包含通用SQL，MySqlMapper是对针MySQL的补充
 */
public interface TkMapper<T> extends Mapper<T>, MySqlMapper<T> {}