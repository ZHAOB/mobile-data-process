package com.xbdx.utils;

import net.sf.json.JSONObject;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Map;


public class HttpPost {


    private final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static   String https(String url, Map<Object, Object> params) throws Exception {
        // 构建请求参数
        String result = "";
        PrintWriter out = null;
        BufferedReader in = null;

        String sendString = "";
        JSONObject json = JSONObject.fromObject(params);
        System.out.println("发送报文:" + json.toString());
        sendString = json.toString();

        System.out.println("ERP连接:" + url);
        System.out.println("发送给ERP信息:" + sendString);

        try {
            trustAllHosts();
            URL url2 = new URL(url);

            HttpsURLConnection urlCon = (HttpsURLConnection) url2.openConnection();
            urlCon.setHostnameVerifier(DO_NOT_VERIFY);
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            urlCon.setRequestMethod("POST");
            urlCon.setRequestProperty("Content-type", "application/json;charset=UTF-8");
            // 发送POST请求必须设置如下两行
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            OutputStream os = urlCon.getOutputStream();
            //参数是键值队  , 不以"?"开始
            os.write(sendString.getBytes());
            //os.write("googleTokenKey=&username=admin&password=5df5c29ae86331e1b5b526ad90d767e4".getBytes());
            os.flush();
            // 发送请求参数
            //out.print(a);
            // flush输出流的缓冲
            //out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(urlCon.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {// 使用finally块来关闭输出流、输入流
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static String http(String url, Map<String, String> reqMap) throws Exception {

        URL u = null;

        HttpURLConnection con = null;

        // 构建请求参数

        //StringBuffer sb = new StringBuffer();
        String sendString = "" ;
        //String tradeCode = params.get(ParamsConfig.keyTradeCode);
        JSONObject json = JSONObject.fromObject(reqMap);
        System.out.println("发送报文:"+json.toString());
        sendString = json.toString();
        /*if(tradeCode.equals(TradeCode.TRANS_WXAPP_PAY) || tradeCode.equals(TradeCode.TRANS_APP_QUERY)
                || tradeCode.equals(TradeCode.TRANS_TYPE_COLLECT) || tradeCode.equals(TradeCode.TRANS_TYPE_QUERY_COLLECT_TXN)){
            JSONObject json = JSONObject.fromObject(params);
            System.out.println("发送报文:"+json.toString());
            sendString = json.toString();
        } else {
            if (params != null) {

                for (Entry<String, String> e : params.entrySet()) {

                    sb.append(e.getKey());

                    sb.append("=");

                    sb.append(e.getValue());

                    sb.append("&");

                }
                sendString = sb.substring(0, sb.length() - 1);
            }
        }*/
        System.out.println("ERP连接:" + url);
        System.out.println("发送给ERP信息:" + sendString);
//        logger.info("ERP连接:" + url);
//        logger.info("发送给ERP信息:" + sb.toString());

        // 尝试发送请求

        try {

            u = new URL(url);

            con = (HttpURLConnection) u.openConnection();

            con.setRequestMethod("POST");

            con.setDoOutput(true);

            con.setDoInput(true);

            con.setUseCaches(false);

//            con.setConnectTimeout(300*1000);
//
//            con.setReadTimeout(300*1000);

            con.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            OutputStreamWriter osw = new OutputStreamWriter(con
                    .getOutputStream(), "UTF-8");

            osw.write(sendString);

            osw.flush();

            osw.close();

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("与服务器连接发生错误:"+e.getMessage());

        } finally {

            if (con != null) {

                con.disconnect();

            }

        }

        // 读取返回内容

        StringBuffer buffer = new StringBuffer();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(con

                    .getInputStream(), "UTF-8"));

            String temp;

            while ((temp = br.readLine()) != null) {

                buffer.append(temp);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("从服务器获取数据失败:"+e.getMessage());

        }

        return buffer.toString();

    }

}