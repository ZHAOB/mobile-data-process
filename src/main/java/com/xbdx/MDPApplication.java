package com.xbdx;

import tk.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xbdx.dao.*")
public class MDPApplication {

	public static void main(String[] args) {
		SpringApplication.run(MDPApplication.class, args);
	}
}
