package com.xbdx.dao;

import com.xbdx.conf.TkMapper;
import com.xbdx.model.DeviceSwitch;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface DeviceSwitchDao extends TkMapper<DeviceSwitch> {

}
